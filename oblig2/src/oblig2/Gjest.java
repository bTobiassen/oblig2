package oblig2;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Gjest extends Kort {
	private GregorianCalendar ugyldighetsDato = new GregorianCalendar();

	public Gjest(String navn, int pin) {
		super(navn, pin, 1);
		ugyldighetsDato.add(Calendar.HOUR, 168);


	}

	@Override
	boolean sjekkePIN(int pin) {

		GregorianCalendar nutid = new GregorianCalendar();
		if (nutid.after(ugyldighetsDato))
			sperretKort = true;

		if (this.isSperret())
			return false;

		if (pin == this.pin)
			return true;
		return false;
	}

	@Override
	public Kort clone() throws CloneNotSupportedException {
		Gjest gjest = (Gjest)super.clone();
		gjest.ugyldighetsDato = (GregorianCalendar) this.ugyldighetsDato.clone();
		
		return gjest;
	}

	@Override
	public String toString() {
		return super.toString() + ", Gjest [ugyldighetsDato=" + ugyldighetsDato + "]]";
	}

}
