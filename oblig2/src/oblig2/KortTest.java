package oblig2;

import java.util.ArrayList;
import java.util.Collections;

public class KortTest {
	public static void main(String[] args) throws CloneNotSupportedException {

		ArrayList<Kort> kortArray = new ArrayList<>();

		Kort k1 = new Ansatt("Ole Bjørnsen", 1234);
		Kort k2 = new Gjest("Marit Olsen", 9999);
		Kort k3 = new Gjest("Marit Daniel", 9999);
		Kort k4 = new Gjest("Rigoberto Andersen", 9999);
		Kort k5 = new Gjest("Markus Olsen", 6547);
		Kort k6 = new Gjest("Marit Nilsen", 4321);
		Kort k7 = k1.clone();
		Kort k8 = k4.clone();
		

		kortArray.add(k1);
		kortArray.add(k2);
		kortArray.add(k3);
		kortArray.add(k4);
		kortArray.add(k5);
		kortArray.add(k6);
		kortArray.add(k7);
		kortArray.add(k8);
		Collections.sort(kortArray);
		
		
		for (Kort kort : kortArray) {
			System.out.println(kort);
			System.out.println("Har  lik referanse med k7    " + (kort == k7 ? "TRUE" : "FALSE"));
			System.out.println("Har ulik referanse med k8    " + (kort.equals(k8) ? "TRUE" : "FALSE"));
		}
		
	}
}