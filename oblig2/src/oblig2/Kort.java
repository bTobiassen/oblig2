package oblig2;

public abstract class Kort implements Comparable<Kort>, Cloneable {
	protected String navn;
	protected String etternavn;
	protected String fornavn;
	protected int pin;
	protected static int staticTeller;
	protected int kortNummer;
	protected int aksesskode;
	protected boolean sperretKort;
	
	public Kort(String navn, int pin, int aksesskode) {
		this.navn = navn;
		this.etternavn= navn.substring(navn.lastIndexOf(" ")+1);
		this.fornavn =navn.substring(0, navn.lastIndexOf(" "));
		this.pin = pin;
		this.aksesskode = aksesskode;
		this.sperretKort = false;
		staticTeller++;
		this.kortNummer = staticTeller;

	}

	public String getNavn() {
		return fornavn+" "+etternavn;
	}

	public boolean isSperret() {
		return sperretKort;
	}

	abstract boolean sjekkePIN(int pin);

	@Override
	public String toString() {
		return "Kort [navn=" + navn + ", etternavn=" + etternavn + ", fornavn=" + fornavn + ", pin=" + pin
				+ ", kortNummer=" + kortNummer + ", aksesskode=" + aksesskode + ", sperretKort=" + sperretKort;
	}

	
	@Override
	public Kort clone() throws CloneNotSupportedException{
		Kort kort = (Kort)super.clone();
		return kort;
	}
	
	@Override
	public int compareTo(Kort kort) {
		if (this.navn == kort.navn)
			return 0;
		else if(this.etternavn != kort.etternavn)
			return this.etternavn.compareTo(kort.etternavn);
		else
			return this.fornavn.compareTo(kort.fornavn);
	}
}
