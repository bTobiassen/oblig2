package oblig2;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Ansatt extends Kort implements Konstanter {
	private double timelonn;
	private double ansinitet;

	public Ansatt(String navn, int pin) {
		super(navn, pin, 1);
		timelonn = 180; //tilfeldige verdier
		ansinitet = 0; //tilfeldige verdier
	}

	public double getTimelonn() {
		return timelonn;
	}

	public void setTimelonn(double timelonn) {
		this.timelonn = timelonn;
	}

	public double getAnsinitet() {
		return ansinitet;
	}

	public void setAnsinitet(double ansinitet) {
		this.ansinitet = ansinitet;
	}

	@Override
	public boolean sjekkePIN(int pin) {
		
		if (this.isSperret())
			return false;

		GregorianCalendar nutid = new GregorianCalendar();
		if (nutid.get(Calendar.HOUR_OF_DAY) < 17 && nutid.get(Calendar.HOUR_OF_DAY) >= 7)
			return true;

		
		if (pin == this.pin)
			return true;
		
		return false;
	}

	@Override
	public void settFornavn(String fornavn) {
		this.fornavn = fornavn;
	}

	@Override
	public void settEtternavn(String etternavn) {
		this.etternavn = etternavn;
	}

	@Override
	public void settFulltNavn(String fornavn, String etternavn) {
		this.navn = fornavn +" "+ etternavn;
	}

	@Override
	public String hentFornavn() {
		return this.fornavn;
	}

	@Override
	public String hentEtternavn() {
		return etternavn;
	}

	@Override
	public String hentFulltNavn() {
		return this.fornavn +" "+ this.etternavn;
	}

	@Override
	public double beregnKreditt() {
		return timelonn * 1.5;		//1.5 tilfeldig verdi
	}

	@Override
	public double beregnBonus() {
		return ansinitet * 5000;	//5000 tilfeldig verdi
	}

	@Override
	public String toString() {
		return super.toString() +", Ansatt [timelønn=" + timelonn + ", ansinitet=" + ansinitet + "]]";
	}
	
	@Override
	public Kort clone() throws CloneNotSupportedException {
		Ansatt ansatt = (Ansatt)super.clone();
		
		return ansatt;
	}

}
