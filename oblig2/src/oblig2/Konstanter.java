package oblig2;

public interface Konstanter {
	void settFornavn(String fornavn);
	void settEtternavn(String etternavn);
	void settFulltNavn(String fornavn, String etternavn);
	String hentFornavn();
	String hentEtternavn();
	String hentFulltNavn();
	double beregnKreditt();
	double beregnBonus();
	
	

}
